(ns plf04.core)

(defn string-E-1
  [s]
  (letfn [(g [c m]
            (cond
              (and (= c \e) (>= (m :c) 3)) {:r false :c (inc (m :c))}
              (and (= c \e) (>= (m :c) 0)) {:r true :c (inc (m :c))}
              :else m))
          (f [xs]
            (if (empty? xs)
              {:r false :c 0}
              (g (first xs) (f (rest xs)))))]
    ((f s) :r)))

(string-E-1 "Hello")
(string-E-1 "Heelle")
(string-E-1 "Heelele")
(string-E-1 "Hill")
(string-E-1 "e")
(string-E-1 "")

(defn string-E-2
  [s]
  (letfn [(g [x]
            (and (>= x 1) (<= x 3)))
          (h [x y]
            (if (= x \e) (inc y) y))
          (f [xs acc]
            (if (empty? xs)
              (g acc)
              (f (rest xs)
                 (h (first xs) acc))))]
    (f s 0)))

(string-E-2 "Hello")
(string-E-2 "Heelle")
(string-E-2 "Heelele")
(string-E-2 "Hill")
(string-E-2 "e")
(string-E-2 "")

;;string-times
(defn string-times-1
  [s n]
  (letfn [(f [cs nn]
           (if (zero? nn)
                 ""
             (str cs (f cs (dec nn)))))]
    (f s n)))
(string-times-1 "Hi" 2)
(string-times-1 "Hi" 3)
(string-times-1 "Hi" 1)
(string-times-1 "Hi" 0)
(string-times-1 "Hi" 5)
(string-times-1 "Oh Boy!" 2)
(string-times-1 "x" 4)
(string-times-1 "" 4)
(string-times-1 "code" 2)
(string-times-1 "code" 3)
(defn string-times-2
  [s n]
  (letfn [(f [cs nn acc]
            (if (zero? nn)
              acc
              (f cs (dec nn)
                 (str acc cs))))]
    (f s n "")))
(string-times-2 "Hi" 2)
(string-times-2 "Hi" 3)
(string-times-2 "Hi" 1)
(string-times-2 "Hi" 0)
(string-times-2 "Hi" 5)
(string-times-2 "Oh Boy!" 2)
(string-times-2 "x" 4)
(string-times-2 "" 4)
(string-times-2 "code" 2)
(string-times-2 "code" 3)
;;front-times
(defn front-times-1
  [s n]
  (letfn [(f [cs nn]
           (if (zero? nn)
             ""
             (str (first cs) (first (rest cs)) (first (rest (rest cs)))
                (f cs (dec nn)))))]
    (f s n)))
(front-times-1 "Chocolate" 2)
(front-times-1 "Chocolate" 3)
(front-times-1 "Abc" 3)
(front-times-1 "Ab" 4)
(front-times-1 "A" 4)
(front-times-1 "" 4)
(front-times-1 "Abc" 0)
(defn front-times-2
  [s n]
  (letfn [(f [cs nn acc]
            (if (zero? nn)
              acc
              (f cs (dec nn)
               (str acc (first cs) (first (rest cs)) (first (rest (rest cs)))))))]
    (f s n "")))
(front-times-2 "Chocolate" 2)
(front-times-2 "Chocolate" 3)
(front-times-2 "Abc" 3)
(front-times-2 "Ab" 4)
(front-times-2 "A" 4)
(front-times-2 "" 4)
(front-times-2 "Abc" 0)
;;count-xx
(defn count-xx-1
  [s]
  (letfn [(g [cc] (if (= "xx" (str (first cc) (first (rest cc))))
                    (inc (f (rest cc)))
                    (f (rest cc))))
          (f [cs]
             (if (empty? cs)
               0
               (g cs)))]
    (f s)))
(count-xx-1 "abcxx")
(count-xx-1 "xxx")
(count-xx-1 "xxxx")
(count-xx-1 "abc")
(count-xx-1 "Hello there")
(count-xx-1 "Hexxo thxxe")
(count-xx-1 "")
(count-xx-1 "Kittens")
(count-xx-1 "Kittensxxx")

(defn count-xx-2
  [s]
  (letfn [(g [cc y] (if (= "xx" (str (first cc) (first (rest cc))))
                         (inc y) y))
          (f [cs acc]
            (if (empty? cs)
              acc
              (f (rest cs) (g cs acc))))]
    (f s 0)))

(count-xx-2 "abcxx")
(count-xx-2 "xxx")
(count-xx-2 "xxxx")
(count-xx-2 "abc")
(count-xx-2 "Hello there")
(count-xx-2 "Hexxo thxxe")
(count-xx-2 "")
(count-xx-2 "Kittens")
(count-xx-2 "Kittensxxx")

;;string-splosion
(defn string-splosion-1
  [s]
  (letfn [(f [cs y]
             (if (or(empty? cs)
                  (>= y (count cs)))
               ""
               (str (subs cs 0 (inc y)) 
                    (f cs (inc y)))))]
    (f s 0)))

(string-splosion-1 "Code")
(string-splosion-1 "abc")
(string-splosion-1 "ab")
(string-splosion-1 "x")
(string-splosion-1 "fade")
(string-splosion-1 "There")
(string-splosion-1 "Kitten")
(string-splosion-1 "Bye")
(string-splosion-1 "Good")
(string-splosion-1 "Bad")

(defn string-splosion-2
  [s]
  (letfn [(f [cs y acc]
             (if (or (empty? cs)
                     (>= y (count cs)))
               acc
               (f cs (inc y)
                  (str acc (subs cs 0 (inc y))))))]
    (f s 0 "")))
(string-splosion-2 "Code")
(string-splosion-2 "abc")
(string-splosion-2 "ab")
(string-splosion-2 "x")
(string-splosion-2 "fade")
(string-splosion-2 "There")
(string-splosion-2 "Kitten")
(string-splosion-2 "Bye")
(string-splosion-2 "Good")
(string-splosion-2 "Bad")

;;array-123
(defn array-123-1
  [vs]
  (letfn [(g [ws] (if (= [1 2 3] (vector (first ws) (first (rest ws)) (first (rest (rest ws)))))
                    (inc (f (into [] (rest ws)))) (f (into [] (rest ws)))))          
          (f [xs] (if (empty? xs)
                    0 (g xs)))
          (h [ys] (> (f ys) 0))
          ]
    (h vs)))
(array-123-1 [1 2 3 1])
(array-123-1 [1 1 2 4 1])
(array-123-1 [1 1 2 1 2 3])
(array-123-1 [1 1 2 1 2 1])
(array-123-1 [1 2 3 1 2 3])
(array-123-1 [1 2 3])
(array-123-1 [1 1 1])
(array-123-1 [1 2])
(array-123-1 [1])
(array-123-1 [])

(defn array-123-2
  [vs]
  (letfn [(h [ws y] (if (= [1 2 3] (vector (first ws) (first (rest ws)) (first (rest (rest ws)))))
                    (inc y) y))
          (g [x] (> x 0))
          (f [xs acc]
            (if (empty? xs)
              (g acc)
              (f (into [] (rest xs)) (h xs acc))))]
    (f vs 0)))
(array-123-2 [1 2 3 1])
(array-123-2 [1 1 2 4 1])
(array-123-2 [1 1 2 1 2 3])
(array-123-2 [1 1 2 1 2 1])
(array-123-2 [1 2 3 1 2 3])
(array-123-2 [1 2 3])
(array-123-2 [1 1 1])
(array-123-2 [1 2])
(array-123-2 [1])
(array-123-2 [])


;;string-x
(defn string-x-1
  [s]
  (letfn [(f [cs y]
             (if (empty? cs)
               ""
               (if (zero? y)
                 (str (first cs) 
                      (f (rest cs) (inc y)))
                 (if (and (= "x" (str(first cs)))
                          (> (count cs) 1))
                   (f (rest cs) (inc y))
                   (str (first cs) (f (rest cs) (inc y)))))))]
    (f s 0)))

(string-x-1 "xxHxix")
(string-x-1 "abxxxcd")
(string-x-1 "xabxxxcdx")
(string-x-1 "xKittenx")
(string-x-1 "Hello")
(string-x-1 "xx")
(string-x-1 "x")
(string-x-1 "")

(defn string-x-2
[s]
(letfn [(f [cs y acc]
           (if (empty? cs)
             acc
             (if (zero? y)
               (f (rest cs) (inc y) (str acc (first cs)))
               (if (and (= "x" (str (first cs)))
                        (> (count cs) 1))
                 (f (rest cs) (inc y) acc)
                 (f (rest cs) (inc y) (str acc (first cs)))))))]
  (f s 0 "")))
(string-x-2 "xxHxix")
(string-x-2 "abxxxcd")
(string-x-2 "xabxxxcdx")
(string-x-2 "xKittenx")
(string-x-2 "Hello")
(string-x-2 "xx")
(string-x-2 "x")
(string-x-2 "")
;;alt-pairs
(defn alt-pairs-1
  [s]
  (letfn [(f [cs x y]
             (if (and (>= y (count cs))
                      (>= x (count cs)))
               ""
               (if(and (< x (count cs))
                       (>= y (count cs) ))
                (str (nth cs x) (f cs (+ 4 x) (+ 4 y)))
                (str (subs cs x (inc y)) (f cs (+ 4 x) (+ 4 y)))
                )))]
    (f s 0 1)))
(alt-pairs-1 "kitten")
(alt-pairs-1 "Chocolate")
(alt-pairs-1 "CodingHorror")
(alt-pairs-1 "yak")
(alt-pairs-1 "ya")
(alt-pairs-1 "y")
(alt-pairs-1 "")
(alt-pairs-1 "ThisThatTheOther")

(defn alt-pairs-2
  [s]
  (letfn [(f [cs x y acc]
             (if (and (>= y (count cs))
                      (>= x (count cs)))
              acc
               (if (and (< x (count cs))
                        (>= y (count cs)))
                 (f cs (+ 4 x) (+ 4 y) (str acc (nth cs x)))
                 (f cs (+ 4 x) (+ 4 y) (str acc (subs cs x (inc y)))) 
                 )))]
    (f s 0 1 "")))
(alt-pairs-2 "kitten")
(alt-pairs-2 "Chocolate")
(alt-pairs-2 "CodingHorror")
(alt-pairs-2 "yak")
(alt-pairs-2 "ya")
(alt-pairs-2 "y")
(alt-pairs-2 "")
(alt-pairs-2 "ThisThatTheOther")

;;string-yak
(defn string-yak-1
  [s]
  (letfn [(f [cs]
             (if (empty? cs)
               ""
               (if (and (= "y" (str(first cs)))
                        (= "k" (str(first(rest(rest cs))))))
                 (f (rest (rest (rest cs))))
                 (str (first cs)(f (rest cs))))))]
    (f s)))
(string-yak-1 "yakpak")
(string-yak-1 "pakyak")
(string-yak-1 "yak123ya")
(string-yak-1 "yak")
(string-yak-1 "yakxxxyak")
(string-yak-1 "HiyakHi")
(string-yak-1 "xxxyakyyyakzzz")

(defn string-yak-2
  [s]
  (letfn [(f [cs acc]
             (if (empty? cs)
               acc
               (if (and (= "y" (str (first cs)))
                        (= "k" (str (first (rest (rest cs))))))
                 (f (rest (rest (rest cs))) acc )
                 (f (rest cs) (str acc (first cs))))))]
  (f s "")))
(string-yak-2 "yakpak")
(string-yak-2 "pakyak")
(string-yak-2 "yak123ya")
(string-yak-2 "yak")
(string-yak-2 "yakxxxyak")
(string-yak-2 "HiyakHi")
(string-yak-2 "xxxyakyyyakzzz")

;;has-271
(defn has-271-1
  [vs]
  (letfn [(f [ws]
             (if (empty? ws)
                 false
                 (if (and (= (first (rest ws)) (+ (first ws) 5))
                          (and (<= (first (rest (rest ws))) (+ (dec (first ws)) 2))
                               (>= (first (rest (rest ws))) (- (dec (first ws)) 2))))
                   true
                   (true? (f (rest ws))))))]
    (f vs)))
(has-271-1 [1 2 7 1])
(has-271-1 [1 2 8 1])
(has-271-1 [2 7 1])
(has-271-1 [3 8 2])
(has-271-1 [2 7 3])
(has-271-1 [2 7 4])
(has-271-1 [2 7 -1])
(has-271-1 [2 7 -2])
(has-271-1 [4 5 3 8 0])
(has-271-1 [2 7 5 10 4])
(has-271-1 [2 7 -2 4 9 3])
(has-271-1 [2 7 5 10 1])
(has-271-1 [2 7 -2 4 10 2])
(has-271-1 [1 1 4 9 0])
(has-271-1 [1 1 4 9 4 9 2])

(defn has-271-2
  [vs]
  (letfn [(f [ws acc]
             (if (empty? ws)
                 (> acc 0)
                 (if (and (= (first (rest ws)) (+ (first ws) 5))
                          (and (<= (first (rest (rest ws))) (+ (dec (first ws)) 2))
                               (>= (first (rest (rest ws))) (- (dec (first ws)) 2))))
                   (f (rest ws) (inc acc))
                   (f (rest ws) acc))))]
    (f vs 0)))

(has-271-2 [1 2 7 1])
(has-271-2 [1 2 8 1])
(has-271-2 [2 7 1])
(has-271-2 [3 8 2])
(has-271-2 [2 7 3])
(has-271-2 [2 7 4])
(has-271-2 [2 7 -1])
(has-271-2 [2 7 -2])
(has-271-2 [4 5 3 8 0])
(has-271-2 [2 7 5 10 4])
(has-271-2 [2 7 -2 4 9 3])
(has-271-2 [2 7 5 10 1])
(has-271-2 [2 7 -2 4 10 2])
(has-271-2 [1 1 4 9 0])
(has-271-2 [1 1 4 9 4 9 2])